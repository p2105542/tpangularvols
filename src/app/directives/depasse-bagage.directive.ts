import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';


@Directive({
  selector: '[appDepasseBagage]',
  standalone: true
})
export class DepasseBagageDirective {

  @Input() appDepasseBagage!: string ;
  @Input() nbBagages!: number;

  constructor(private el: ElementRef, private renderer: Renderer2) {}
  ngOnInit() {
      this.updateBackgroundColor();
  }

  private maxBagages(): number {
    let nbBagagesMax: number;
    switch (this.appDepasseBagage) {
      case 'BUSINESS':
        nbBagagesMax = 2;
        break;
      case 'STANDARD':
        nbBagagesMax = 1;
        break;
      case 'PREMIUM':
        nbBagagesMax = 3;
        break;
      default:
        nbBagagesMax = 1;
        break;
    }
    return nbBagagesMax;
  }
    
  
  private updateBackgroundColor() {
    const color = 'red';
    if (this.nbBagages > this.maxBagages()) {
      this.renderer.setStyle(this.el.nativeElement, 'background-color', color);
    }
  }

}
