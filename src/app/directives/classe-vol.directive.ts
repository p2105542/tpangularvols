import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  standalone: true,
  selector: '[appClasseVol]',
})
export class ClasseVolDirective implements OnInit {
  @Input() appClasseVol!: string ;

  constructor(private el: ElementRef, private renderer: Renderer2) {}
  ngOnInit() {
    this.updateTextColor();
  }

  private updateTextColor() {
    let color: string;

    switch (this.appClasseVol) {
      case 'BUSINESS':
        color = 'red';
        break;
      case 'STANDARD':
        color = 'blue';
        break;
      case 'PREMIUM':
        color = 'green';
        break;
      default:
        color = 'black';
        break;
    }

    this.renderer.setStyle(this.el.nativeElement, 'color', color);
  }
}
