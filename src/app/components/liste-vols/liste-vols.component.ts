import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { CommonModule } from '@angular/common';
import { VolComponent } from '../vol/vol.component';

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [CommonModule, VolComponent],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {

  @Input() listeVols: Vol[] = [];
  @Output() e_volSelected = new EventEmitter<Vol>;
  @Input() type!: string;

  envoyerVol(vol:Vol){
    this.e_volSelected.emit(vol);
  }

}
