import { Component, Input } from '@angular/core';
import { IPassager } from '../../models/passager.model';
import { CommonModule } from '@angular/common';
import { PassagerComponent } from '../passager/passager.component';
import { FormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-liste-passagers',
  standalone: true,
  imports: [CommonModule,PassagerComponent, FormsModule, MatSlideToggleModule],
  templateUrl: './liste-passagers.component.html',
  styleUrls: ['./liste-passagers.component.scss']
})
export class ListePassagersComponent {

  @Input() listePassagers!: IPassager[];
  
  afficherPhotos: boolean = false;
}
