import { Component, Input } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import { IFiltres } from '../../models/filtres.model';
import { VolService } from '../../services/vol.service';
import { Vol } from '../../models/vol.model';
import { Passager } from '../../models/passager.model';
import { PassagerService } from '../../services/passager.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent {

  filtres: IFiltres | undefined;
  vols: Vol[] = [];
  volChoisit : Vol | undefined;
  listePassagersVol: Passager[] = [];
  message: string = "pas encore de passagers generes"

  type!: string;

  constructor(
    private volService: VolService,
    private passagerService: PassagerService,
    private _activatedRoute: ActivatedRoute
  ) { };

  ngOnInit(): void {
    this._activatedRoute.data.subscribe(data => {
      this.type = data['type'];
    })
  };

  onFiltresChanged(filtres: IFiltres) {
    this.filtres = filtres;
    this.getVols(this.filtres);
  }

  getVols(filtres: IFiltres) {
    const debutTimestamp = new Date(filtres.debut).getTime() / 1000; 
    const finTimestamp = new Date(filtres.fin).getTime() / 1000; 

    if (this.type === 'DECOLLAGES') {
      this.volService.getVolsDepart(filtres.aeroport.icao, debutTimestamp, finTimestamp).subscribe({
        next: (vols: Vol[]) => {
          this.vols = vols;
        }
      });
      return;
    }

    this.volService.getVolsArrivee(filtres.aeroport.icao, debutTimestamp, finTimestamp).subscribe({
      next: (vols: Vol[]) => {
        this.vols = vols;
      }
    });
  }

  onVolChoisit(evol:Vol){
    if (evol && evol.icao) {
    this.message="nmlt passagers generes !!!!"
    this.volChoisit = evol;
    this.passagerService.getPassagers(evol.icao).subscribe({
      next: (listePassagersVol: Passager[]) => {
        this.listePassagersVol = listePassagersVol;
      },
      error: (err) => {
        this.message='Error occurred while fetching passagers: '+err + "icao :"+evol.icao;
      }
    });
  } else {
    this.message='Error: Flight or ICAO code is undefined.';
  }

  }

}
