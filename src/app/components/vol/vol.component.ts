import { Component, Input } from '@angular/core';
import { Vol } from '../../models/vol.model';
import {MatIconModule} from '@angular/material/icon';
@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [MatIconModule],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {
  @Input() volU!: Vol;
  @Input() type!: string;

  get logo() {
    switch (this.volU.compagnie) {
      case 'Air France':
        return '../../assets/Air France.png';
      case 'Air France Hop':
        return '../../assets/Air France Hop.png';
      case 'Transavia France':
        return '../../assets/Transavia France.png';
      default:
        return 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Inconnu_%28Stenodus_nelma%29_%28line_art%29_%28PSF_I-470003_%28cropped%29%29.png/640px-Inconnu_%28Stenodus_nelma%29_%28line_art%29_%28PSF_I-470003_%28cropped%29%29.png';
    }
  }
}
