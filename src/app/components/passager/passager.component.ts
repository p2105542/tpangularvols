import { Component, Input } from '@angular/core';
import { Passager } from '../../models/passager.model';
import { MatIcon } from '@angular/material/icon';
import { ClasseVolDirective } from '../../directives/classe-vol.directive';
import { DepasseBagageDirective } from '../../directives/depasse-bagage.directive';

@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [MatIcon, ClasseVolDirective,DepasseBagageDirective],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input() passager!:Passager;
  @Input() afficherPhotos!: boolean;
}
