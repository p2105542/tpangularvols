import { RouterModule, Routes } from '@angular/router';
import { ViewAirFranceComponent } from './components/view-airfrance/view-airfrance.component';


export const routes: Routes = [
  { path: 'decollages', component: ViewAirFranceComponent, data: { type: 'DECOLLAGES' } },
  { path: 'atterrissages', component: ViewAirFranceComponent, data: { type: 'ATTERRISSAGES' } },
  { path: '', redirectTo: '/decollages', pathMatch: 'full' }
];



